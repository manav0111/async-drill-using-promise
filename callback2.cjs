// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.


const callback2 = (id, Listdata) => {
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            const ListInfo=Listdata[id];
            
            if(ListInfo.length)
            {

                resolve(ListInfo);
            
    
            }
            else
            {
                reject("List id is Not Match");
            }
        
        }, 2 * 1000);

    })
  };
  

  module.exports = callback2;
  