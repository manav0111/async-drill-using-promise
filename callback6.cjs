// Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.
// Get information from the Thanos boards
// Get all the lists for the Thanos board
// Get all cards for all lists simultaneously


const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");

const callback6 = (name, Boardsdata, Listdata, Carddata) => {
  return new Promise((resolve, reject) => {
    let id;
    Boardsdata.forEach((Object) => {
      if (Object.name == name) {
        id = Object.id;
      }
    });
    console.log(id);

    callback1(id, Boardsdata)
      .then((data) => {
        console.log(data);
      })
      .catch((err) => console.log(err));

    callback2(id, Listdata)
      .then((data) => {
          
          data.forEach((obj) => {
              console.log(`Id - ${obj.id}, Name - ${obj.name} `);
            });
      })
      .catch((err) => reject(err));

      const keys=Object.keys(Listdata);
      let ID=Listdata[keys[0]][1].id;;

        callback3(ID, Carddata).then((data)=>{
            data.forEach((obj) => {
                console.log(`Id - ${obj.id}, Description - ${obj.description} `);
              });

              if(data.length)
              {
                resolve("Successfull");
              }

          }).catch((err)=>console.log(err));


  });
};

module.exports = callback6;