const fs=require('fs');
const path=require('path');


let filepath=path.join(
    __dirname,
    "boards_1.json"
);


let board=fs.readFileSync(filepath,"utf8");

const Boardsdata=JSON.parse(board);
// console.log(Boardsdata);

filepath=path.join(
    __dirname,
    "list_1.json"
);
let List=fs.readFileSync(filepath,"utf8");

const Listdata=JSON.parse(List);
// console.log(Listdata);

filepath=path.join(
    __dirname,
    "cards_1.json"
);

const Card=fs.readFileSync(filepath,"utf8");

const Carddata=JSON.parse(Card);
// console.log(Carddata);

module.exports={Boardsdata,Listdata,Carddata};