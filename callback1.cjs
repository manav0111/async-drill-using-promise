// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.

const callback1 = (id, Boardsdata) => {

    return new Promise((resolve,reject)=>{
        setTimeout(() => {
          const boardinfo = Boardsdata.filter((obj) => {
            return obj.id == id;
          });
      
          if (boardinfo.length) {
            resolve(boardinfo);
            // cb(null, boardinfo);
          } else {
            reject("Board id Not match");
            // cb("Board id Not match", null);
          }
      
          // Your code here
        }, 2 * 1000);

    })
  };
  
  module.exports = callback1;
  