// Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.
// Get information from the Thanos boards
// Get all the lists for the Thanos board
// Get all cards for the Mind and Space lists simultaneously
const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");

const callback5 = (name, Boardsdata, Listdata, Carddata) => {
  return new Promise((resolve, reject) => {
    let id;
    Boardsdata.forEach((Object) => {
      if (Object.name == name) {
        id = Object.id;
      }
    });
    console.log(id);

    callback1(id, Boardsdata)
      .then((data) => {
        console.log(data);
      })
      .catch((err) => console.log(err));

    callback2(id, Listdata)
      .then((data) => {
        let IDArray = [];

        data.forEach((obj) => {
          if (obj.name == "Mind" || obj.name == "Space") {
            console.log(obj.id, obj.name);
            IDArray.push(obj.id);
          }
          console.log(`Id - ${obj.id}, Name - ${obj.name} `);
        });

        let index = 0;

        for (index = 0; index < IDArray.length; index++) {
          callback3(IDArray[index], Carddata)
            .then((data) => {
              data.forEach((obj) => {
                console.log(
                  `Id - ${obj.id}, Description - ${obj.description} `
                );
              });
            })
            .catch((err) => console.log(err));
        }
      })
      .catch((err) => reject(err));
  });
};

module.exports = callback5;
